package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class MyClass {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(4, "com.example.vinitha.greendaoapp.db");
        addNote(schema);

        //addSample(schema);
        DaoGenerator dg = new DaoGenerator();
        dg.generateAll(schema, "./app/src/main/java");
    }

    public static void addNote(Schema schema)
    {
        Entity Book = schema.addEntity("Book");
        Book.addIdProperty().primaryKey().autoincrement();
        Book.addStringProperty("Bname");

        Entity author = schema.addEntity("Author");
        author.addIdProperty().primaryKey().autoincrement();
        author.addStringProperty("authorname");

        Property authorid=Book.addLongProperty("authorid").getProperty();
        Book.addToOne(author, authorid);
    }
}
