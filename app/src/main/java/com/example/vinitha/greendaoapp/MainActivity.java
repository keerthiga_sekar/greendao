package com.example.vinitha.greendaoapp;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.vinitha.greendaoapp.db.Author;
import com.example.vinitha.greendaoapp.db.AuthorDao;
import com.example.vinitha.greendaoapp.db.Book;
import com.example.vinitha.greendaoapp.db.BookDao;
import com.example.vinitha.greendaoapp.db.DaoMaster;
import com.example.vinitha.greendaoapp.db.DaoSession;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

//import com.example.vinitha.greendaoapp.db.BookDao;

public class MainActivity extends AppCompatActivity
{



   private static BookDao bookDao;
   private static AuthorDao authorDao;
    public Button b;
    public ArrayList<String> result;
    EditText ed,edcount;
   String TAG="mainclas";
  public ListView ls;

   public static BookDao getBookDao()
    {
        return bookDao;
    }

   public static AuthorDao getAuthorDao()
    {
        return authorDao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ls=(ListView)findViewById(R.id.listv);
        result = new ArrayList<String>();
        ed=(EditText)findViewById(R.id.editText);
        edcount=(EditText)findViewById(R.id.editText2);
        b=(Button)findViewById(R.id.button);
        DaoMaster.DevOpenHelper helper=new DaoMaster.DevOpenHelper(this, "notes-db", null);
        SQLiteDatabase db=helper.getWritableDatabase();
        DaoMaster daoMaster= new DaoMaster(db);
       DaoSession daoSession= daoMaster.newSession();
       bookDao = daoSession.getBookDao();
       authorDao =daoSession.getAuthorDao();

        Button btnSignIn = (Button) findViewById(R.id.button);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertrows();

                Cursor c = bookDao.getDatabase().rawQuery(
                        "SELECT * FROM BOOK", null);
                if (c.moveToFirst()) {
                    do {
                        result.add(c.getString(0));
                    } while (c.moveToNext());
                }
                c.close();
                print(result);
            }
        });

    }


     public void print(ArrayList<String> result) {
         ArrayAdapter adapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,result);
         ls.setAdapter(adapter);

    }
    private void insertrows()
    {
        Log.d(TAG, "fine til now");
        Author ath1=new Author();
       String h=ed.getText().toString();
      String h1=edcount.getText().toString();
        //ath1.setId((long) 9);
       ath1.setAuthorname(h1);

        //ath.setNoteid(notte.getId());
        authorDao.insert(ath1);
      Log.d(TAG, "authorid:" + ath1.getId());
      Book notte=new Book();
      notte.setAuthorid(ath1.getId());
  //      notte.setAuthorid(ath1.getId());
        Log.d(TAG, "fine til now");
       // String h=ed.getText().toString();
       // String h1=edcount.getText().toString();

        // String h=ed.getText().toString();
      //  notte.setId((long) 1);
       //notte.getAnoteid();
        notte.setBname(h);
      // String h1=edcount.getText().toString();
       // notte.setPlace("neyveli");
        Log.d(TAG, "fine til now");
       // notte.setAuthorid(ath.getId());
       // notte.setId(9);
         bookDao.insert(notte);
        final String SQL_QUERY=" SELECT "+ BookDao.Properties.Bname +" FROM "+bookDao;


        DaoMaster.DevOpenHelper helper=new DaoMaster.DevOpenHelper(this, "notes-db", null);
        SQLiteDatabase db=helper.getWritableDatabase();
        DaoMaster daoMaster= new DaoMaster(db);
        DaoSession daoSession= daoMaster.newSession();


       QueryBuilder qb= bookDao.queryBuilder().limit(1);


              // .where(NoteDao.Properties.Name.eq("keer"));
             //  .or(NoteDao.Properties.Country.eq("VINI"));
            //  .where(NoteDao.Properties.Name.eq("divya"));
              List ks= qb.list();

        System.out.println(ks);

        System.out.println("the insrted recoed:"+ks);
        Log.d(TAG, "finished insertion");



/*
        Note nop=new Note();
       // nop.setId(5);
        nop.setName("keer");
        nop.setCountry("neyveli");
        noteDao.insert(nop);
        Log.d(TAG, "finished insertion"); */

               // Log.d(TAG, notte.getName());
      //  Log.d(TAG,notte.getCountry());

       // Log.d("DaoExample", "Inserted new note, ID: " + notte.getId());

    }


}